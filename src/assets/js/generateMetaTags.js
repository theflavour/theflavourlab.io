import metadata from '../../../content/metadata.json';

function generateMetaTags(seo = {}, path, defaultTitle) {
  const meta = [];

  if (seo.title) {
    meta.push({
      key: 'og:title',
      property: 'og:title',
      content: `${seo.title} – ${metadata.siteName}`,
    });
  } else if (defaultTitle) {
    meta.push({
      key: 'og:title',
      property: 'og:title',
      content: `${defaultTitle} – ${metadata.siteName}`,
    });
  }

  if (seo.description) {
    meta.push({
      key: 'og:description',
      property: 'og:description',
      content: seo.description,
    });
    meta.push({
      key: 'description',
      name: 'description',
      content: seo.description,
    });
  }

  if (seo.image) {
    meta.push({
      key: 'og:image',
      property: 'og:image',
      content: `${metadata.siteUrl}${seo.image.src}`,
    });

    if (seo.imageAlt) {
      meta.push({
        key: 'twitter:image:alt',
        name: 'twitter:image:alt',
        content: seo.imageAlt,
      });
    }
  }

  if (seo.cardStyle) {
    meta.push({
      key: 'twitter:card',
      name: 'twitter:card',
      content: seo.cardStyle,
    });
  }

  if (seo.keywords && seo.keywords.length > 0) {
    meta.push({
      key: 'keywords',
      name: 'keywords',
      content: seo.keywords.join(','),
    });
  }

  if (seo.noindex) {
    meta.push({
      key: 'robots',
      name: 'robots',
      content: 'noindex',
    });
  }

  meta.push({
    key: 'og:url',
    property: 'og:url',
    content: `${metadata.siteUrl}${path}`,
  });

  return meta;
}

export default generateMetaTags;
