import metadata from '../../../content/metadata.json';

export default function generateCanonicalLinks(url, path) {
  if (url) return [{ rel: 'canonical', href: url }];
  return [{ rel: 'canonical', href: `${metadata.siteUrl}${path}` }];
}
