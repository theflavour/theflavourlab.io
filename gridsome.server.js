// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = (api) => {
  api.loadSource(({ addSchemaTypes, schema }) => { // eslint-disable-line no-unused-vars
    addSchemaTypes(`
      # Seo Type
      type SeoData {
        title: String
        description: String
        image: Image
        imageAlt: String
        cardStyle: String
        keywords: [String]
        canonicalUrl: String
        noindex: Boolean
      }

      # Cta Type
      type Cta {
        icon: String
        color: String
        label: String
        target: String
      }

      # Gradient Type
      type Gradient {
        colorOne: String
        colorTwo: String
      }

      # Tagged Image Type
      type TaggedImage {
        src: Image,
        alt: String,
        title: String
      }

      # Gated Asset Type
      type GatedAsset implements Node {
        id: String
        color: String
        dark: Boolean
        title: String
        image: Image
        imageAlt: String
        downloadLink: File
        formId: String
      }

      # Open Position Type
      type OpenPosition implements Node {
        color: String
        icon: String
        title: String
        content: String
        link: String
      }

      # Project Blocks Type
      type ProjectBlocks {
        template: String
        alt: String
        client: String
        color: String
        customGradient: Boolean
        dark: Boolean
        faded: Boolean
        image: Image
        images: [TaggedImage]
        name: String
        negativeMargin: Boolean
        src: Image
        text: String
        title: String
      }

      # Story Blocks Type
      type StoryBlocks {
        template: String
        alt: String
        client: String
        color: String
        customGradient: Boolean
        dark: Boolean
        faded: Boolean
        image: Image
        images: [TaggedImage]
        imageFirst: Boolean
        name: String
        negativeMargin: Boolean
        src: Image
        text: String
        title: String
      }

      # Page Content Type
      type PageContent implements Node @infer {
        benefits: [String]
        seo: SeoData
      }

      # Topic Type
      type Topic implements Node @infer {
        seo: SeoData
      }

      # Project Type
      type Project implements Node @infer {
        seo: SeoData
        blocks: [ProjectBlocks]
      }

      # Story Type
      type Story implements Node @infer {
        seo: SeoData
        blocks: [StoryBlocks]
      }

      # Anchor Menu Entry type
      type AnchorMenuEntry {
        label: String,
        anchor: String,
      }

      # Language Entry Type
      type LanguageEntry {
        label: String,
        url: String,
      }

      # Landing Page Type
      type LandingPage implements Node {
        color: String,
        content: [Block],
        cta: Cta,
        dark: Boolean,
        lang: String,
        langs: [LanguageEntry],
        logo: Image,
        menu: [AnchorMenuEntry],
        seo: SeoData,
        title: String,
      }

      # Block Types need to implement the Block interface described below
      # Type for text columns
      type Column {
        body: String
      }
      type TextBlock implements Block {
        template: String!
        id: String
        background: Boolean
        dark: Boolean
        title: String
        columns: [Column] # may contain HTML
      }

      type HeroBlock implements Block {
        template: String!
        id: String
        overline: String
        title: String
        blurb: String # may contain inline HTML
        showLogo: Boolean
        negativeMargin: Boolean
        color: String
        dark: Boolean
      }

      type TextAndImageBlock implements Block {
        template: String!
        id: String
        alt: String
        background: Boolean
        dark: Boolean
        imageFirst: Boolean
        imageFocus: Boolean
        src: Image
        title: String
        body: String # may contain HTML
        cta: Cta
      }

      # Type for lists of features
      type FeatureItem {
        title: String
        body: String # may contain inline HTML
        icon: String
      }
      type FeaturesBlock implements Block {
        template: String!
        id: String
        title: String
        features: [FeatureItem]
        dark: Boolean
      }

      # A block for displaying stepped content
      type StepItem {
        icon: String
        title: String
        body: String # may contain inline HTML
      }

      type StepsBlock implements Block {
        template: String!
        id: String
        title: String
        steps: [StepItem]
      }

      type SeparatorBlock implements Block {
        template: String!
        id: String
        alt: String
        color: String
        content: String # may contain inline HTML
        cta: Cta
        customGradient: Boolean # This option does nothing except on Projects, where it causes the background color to be based on the project colors, should be refactored
        dark: Boolean
        faded: Boolean
        negativeMargin: Boolean
        src: Image
        title: String
      }

      type QuoteBlock implements Block {
        template: String!
        id: String
        client: String
        color: String
        customGradient: Gradient
        name: String
        quote: String
        src: Image
      }

      type VideoBlock implements Block {
        template: String!
        id: String
        color: String
        dark: Boolean
        fullWidth: Boolean
        videoId: String
        videoThumbnail: Image
        title: String
        columns: [Column]
      }

      # A block and type for showing clickable cards
      type Card {
        asset: File # if provided should be a link to a file
        borderColor: String
        color: String
        dark: Boolean
        href: String
        icon: String
        image: Image
        size: String
        body: String # may contain inline HTML
        title: String
        type: String
      }

      type CardsBlock implements Block {
        template: String!
        id: String
        title: String
        background: Boolean
        dark: Boolean
        cards: [Card]
      }

    `);

    // This is needed so we can properly associate the Type of each block
    // See: https://stackoverflow.com/questions/52088172/how-do-you-handle-an-array-of-multiple-types-ex-different-content-blocks-in-g
    // It needs to be done like this since addSchemaResolvers can’t find a Block union/interface to add the custom resolver to
    // then associate the template (the name of the block-front-matter-template in Forestry)
    // with the correct type
    addSchemaTypes([
      schema.createInterfaceType({
        name: 'Block',
        fields: {
          template: 'String!',
          id: 'String',
        },
        resolveType(obj) { // eslint-disable-line consistent-return
          if (obj.template === 'block-text') return 'TextBlock';
          if (obj.template === 'block-hero') return 'HeroBlock';
          if (obj.template === 'block-text-and-image') return 'TextAndImageBlock';
          if (obj.template === 'block-features') return 'FeaturesBlock';
          if (obj.template === 'block-steps') return 'StepsBlock';
          if (obj.template === 'block-separator') return 'SeparatorBlock';
          if (obj.template === 'block-quote') return 'QuoteBlock';
          if (obj.template === 'block-video') return 'VideoBlock';
          if (obj.template === 'block-cards') return 'CardsBlock';
        },
      }),
    ]);
  });

  api.createPages(({ createPage }) => { // eslint-disable-line no-unused-vars
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });
};
