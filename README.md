# Flavour Communication

## Website V2

This is the new website for Flavour Kommunikation, built in [Vue.js](https://vuejs.org)
with [Gridsome](https://gridsome.org).

It uses Git LFS for versioning binary files, so make sure it is installed and
configured properly on your device. Since this project uses `yorkie`, its hooks
might conflict with the hooks Git LFS needs to set up. __You might have to manually
merge the hook files after cloning this repo for everything to work properly__!
