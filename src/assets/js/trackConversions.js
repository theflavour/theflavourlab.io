/* globals gtag */

/**
 * A function to track Google conversions on specific pages
 * @param {string} campaign - The campaign the events should be tracked for, will be used to only track specific routes
 * @param {string} path - The path of the current route
 */
export default function trackGoogleConversions(campaign, path) {
  if (typeof gtag !== 'function') return; // if gtag is not defined, we cannot track
  switch (campaign) {
    case 'klinikmarketing':
      if (
        path === '/kliniken-und-soziales/'
        || path === '/projekte/'
        || path === '/kliniken-und-soziales/mitarbeiterbindung-und-motivation/'
        || path === '/kliniken-und-soziales/zeitgemaesse-patientengewinnung/'
        || path === '/kliniken-und-soziales/neupositionierung-und-profilschaerfung/'
        || path === '/kliniken-und-soziales/etablierung-neuer-geschaeftsfelder/'
        || path === '/kliniken-und-soziales/zusammenschluesse-und-fusionen/'
      ) gtag('event', 'conversion', { send_to: 'AW-986059024/GMjiCNa-g4QDEJCimNYD' });
      break;
    default:
  }
}
