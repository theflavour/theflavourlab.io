<div style="max-width: 60rem; margin: 0 auto;">

# Anleitung: Neues Icon hinzufügen

> In dieser Anleitung geht es darum, wie ein neues Icon zur Verwendung auf der
> gesamten Seite z.B. in Buttons aufbereitet und importiert werden kann.


## 1. Das Icon aufbereiten

### Benötigte Dateien und Formate

Icons auf der Flavour-Website sind als monochromes SVG direkt in den Quellcode
eingebettet, damit wir sie beliebig einfärben können. Die Grundlage dafür
stellen SVG-Dateien die in einem **24x24px** großen Quadrat angelegt sind. Diese
Icons haben eine Strichstärke von 3px und keine Füllung, keine abgerundeten
Kanten und keine runden Strich-Enden.


![Eine Infografik, die die Icon-Anatomie zeigt](https://flavour-kommunikation.de/___documentation/icon_anatomy.jpg)


Im Augenblick sind alle bestehenden Icons in der Figma-Datei "Website 2.0 /
Entwicklung" auf der Seite "Icons" abgelegt, es bietet sich hier natürlich an,
auch alle neuen Icons an diesem Ort zu gestalten. Das hat zusätzlich den Vorteil,
dass neue Icons nicht extra exportiert werden müssen, sondern einfach über
Rechtsklick → Copy/Paste → Copy as SVG als Code kopiert werden können.


![Ein Screenshot des Copy as SVG Menüs](https://flavour-kommunikation.de/___documentation/figma_copy_svg.jpg)


**Wichtig hierbei:** wird das Icon so kopiert *muss* der "Rahmen" des Icons ein
24x24px Quadrat sein. Das kann erreicht werden, indem der Pfad ausgewählt
und mit Cmd/Strg + Alt + G in einen "Frame" gruppiert wird. Dieser Frame kann
bei gedrückter Cmd/Strg-Taste auf die 24px skaliert werden, ohne, dass sich der
Inhalt verändert, bzw. verschiebt.


![Eine Infografik, die zeigt, welcher Rahmen der Korrekte ist](https://flavour-kommunikation.de/___documentation/figma_boundingbox.jpg)


### SVG für das Web optimieren

Die exportierten / kopierten SVGs sollten auch wie in der Anleitung "Neue
Kundenlogos" beschrieben mit [SVGOMG](https://jakearchibald.github.io/svgomg/)
optimiert werden. Wurde der Icon-Code aus Figma kopiert, kann dieser bei "Paste
Markup" eingefügt werden.


![Ein Screenshot von SVGOMG für die Option Paste Markup](https://flavour-kommunikation.de/___documentation/svgomg_pastemarkup.jpg)


Die Einstellungen für SVGOMG sind die gleichen wie in der Anleitung "Neue
Kundenlogos":


![Die aktiven Features in SVGOMG](https://flavour-kommunikation.de/___documentation/svgomg_features.jpg)


## 2. Das Icon im CMS einfügen

Neue Icons können im CMS im Bereich "Verfügbare Icons" hinzugefügt werden.
In diesem Bereich befinden sich zwei Listen: eine Liste mit IDs und eine zweite
Liste mit den eigentlichen Logo-Elementen. Diese zweite Liste hat einen grünen
Button "Add Icon", mit dem ein neues Icon hinzugefügt werden kann.


![Der Add Icon Button in Forestry](https://flavour-kommunikation.de/___documentation/forestry_add_icon.jpg)


In dem Dialog, der sich daraufhin öffnet, befinden sich zwei Felder:

1. `ID` für die **einzigartige** Id des Icons – typischerweise der Name des Icons
  in Kleinbuchstaben und ohne Sonderzeichen. Leerzeichen **müssen** mit
  Bindestrichen ersetzt werden. Aus "Arrow Right" wird also z.B. "arrow-right".
2. `SVG Code` für den Code des SVGs


![Die zwei Felder zum anlegen eines neuen Icons](https://flavour-kommunikation.de/___documentation/forestry_steps_icon.jpg)


**Wichtig:** der Code, der bei 2. in das Feld eingetragen wurde darf keine `fill`,
`stroke`, oder `stroke-width` Attribute enthalten. Falls also etwas wie
`<path stroke="#000" …/>` auftauchen sollte, muss es so editiert werden, dass nur
`<path …/>` dort steht. **Außerdem muss das umgebende `<svg></svg>` entfernt
werden!**


![Infografik, welcher Text bestehen bleiben darf und welcher nicht](https://flavour-kommunikation.de/___documentation/forestry_add_icon_code.jpg)


> **Tipp**: wenn in SVGOMG die Option "Prettify Markup" ausgewählt wird, lässt
> sich der Code im Tab "Markup" einfacher ohne das umgebende `<svg></svg>`
> kopieren:


![Infografik, wie das Kopieren des Icon Codes leichter von Statten gehen kann](https://flavour-kommunikation.de/___documentation/svgomg_tipp_markup.jpg)



## 3. Das Logo zur Verwendung freigeben

Damit das Icon anschließend auf der Seite verwendet werden kann, muss die Id,
die im zweiten Schritt in das "Id"-Feld eingetragen wurde in die erste Liste auf
der Seite eingefügt werden. Hier bitte doppelt prüfen, ob die Id *exakt gleich*
ist – oder einfach den Wert kopieren und einfügen.


![Das Id Textfeld in Forestry](https://flavour-kommunikation.de/___documentation/forestry_id_field_icon.jpg)


Abschließend müssen diese Änderungen noch über den "Speichern"-Button oben
rechts abgespeichert werden.


![Der Speichern-Button in Forestry](https://flavour-kommunikation.de/___documentation/forestry_save.jpg)


**Achtung**: es kann vorkommen, dass die Icons erst dann in einer Vorschau
angezeigt werden, wenn der Preview-Server neu gestartet wird. So lang die IDs und
der SVG-Code im angegebenen Format richtig eingetragen wurde, kann davon
ausgegangen werden, dass auf der veröffentlichten Website alles stimmt.

</div>
