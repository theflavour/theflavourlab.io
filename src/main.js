// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '@/layouts/Default.vue';

import '@fontsource/dm-sans/400.css';
import '@fontsource/dm-sans/500.css';
import '@fontsource/dm-sans/700.css';
import '@fontsource/dm-serif-display';

import 'normalize.css';
import '@/assets/styles/base.styl';
import '@/assets/styles/nprogress.styl';

export default (Vue, { appOptions, router, head, isClient }) => { // eslint-disable-line no-unused-vars, object-curly-newline
  // import base components for convenience
  const requireComponent = require.context('./components', false, /Flavour[A-Z]\w+\.(vue|js)$/);

  requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);
    const componentName = fileName.split('/').pop().replace(/\.\w+$/, '');
    Vue.component(componentName, componentConfig.default || componentConfig);
  });

  // Set up scrollRestoration
  router.options.scrollBehavior = function customScrollBehavior(to, from, savedPosition) { // eslint-disable-line no-param-reassign
    let position = {};
    if (savedPosition) position = savedPosition;
    else if (to.hash) {
      if (to.path === from.path) return { selector: to.hash, offset: { x: 0, y: 192 } };
      position = { selector: to.hash, offset: { x: 0, y: 192 } };
    } else position = { x: 0, y: 0 };
    return new Promise((resolve) => {
      this.app.$root.$once('triggerScroll', () => {
        resolve(position);
      });
    });
  };

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);

  // Set default language on html to de
  head.htmlAttrs = { lang: 'de' }; // eslint-disable-line no-param-reassign

  // Add a custom noscript style tag, fixes g-image and missing noscript support when JS is disabled
  head.noscript = [ // eslint-disable-line no-param-reassign
    { innerHTML: '<style>.g-image--loading{ display: none !important; }.nojs { display: block !important; }</style>' },
  ];

  // Needed for enabling plausible 404 and other custom events detection
  head.script.push({
    innerHTML: 'window.plausible = window.plausible || function() { (window.plausible.q = window.plausible.q || []).push(arguments) }',
  });

  // Set up global data fields
  /* eslint-disable no-param-reassign */
  appOptions.data.mobile = false;
  appOptions.data.pageColor = 'strawberry';
  appOptions.data.pageDark = true;
  /* eslint-enable no-param-reassign */
};
