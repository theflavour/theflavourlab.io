---
color: plum
dark: false
overline: Legales
titel: Impressum
einleserHTML: Angaben gemäß § 5 TMG

---
**Handelsregister**: HRB 131198
**Registergericht**: Amtsgericht München

**Vertreten durch**:
Roland Braun
Emdenstraße 88
81735 München

### Kontakt

Telefon: +49 (0) 89 4890080
Telefax: +49 (0) 89 4890085
E-Mail: [hello@flavour-kommunikation.de](mailto:hello@flavour-kommunikation.de)

### Postadresse

Flavour Kommunikation GmbH
Postbox 656518
96035 Bamberg

### Umsatzsteuer-ID

Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz:
DE 205 373 062

### Verbraucher­streit­beilegung / Universal­schlichtungs­stelle

Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

Quelle: [e-recht24.de](https://www.e-recht24.de)
