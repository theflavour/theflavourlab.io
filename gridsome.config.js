// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const postcssObjectFit = require('postcss-object-fit-images'); // eslint-disable-line import/no-extraneous-dependencies
const metadata = require('./content/metadata.json');

module.exports = {
  css: {
    loaderOptions: {
      postcss: {
        plugins: [postcssObjectFit],
      },
    },
  },
  icon: {
    favicon: './src/assets/images/favicon.png',
    touchicon: './src/assets/images/touchicon.png',
  },
  metadata,
  outputDir: 'public',
  siteUrl: metadata.siteUrl,
  siteName: metadata.siteName,
  siteDescription: metadata.siteDescription,
  siteKeywords: metadata.siteKeywords,
  templates: {
    GatedAsset: '/gated-content/:title',
    LandingPage: '/lp/:lang/:title',
    Project: '/projekt/:logo-:title',
    Story: '/story/:logo-:title',
    TextPage: '/:titel',
    Topic: '/:bereiche__0/:titel',
  },
  titleTemplate: `%s – ${metadata.siteName}`,
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'PageContent',
        path: './content/pages/*.json',
        refs: {
          projekte: 'Project',
          stories: 'Story',
          mitarbeiter: 'Employee',
          gatedAsset: 'GatedAsset',
        },
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'TextPage',
        path: './content/pages/*.md',
        resolveAbsolutePaths: true,
        remark: {
          autolinkHeadings: false,
          fixGuillements: false,
          imageQuality: 100,
          plugins: [
            'remark-breaks',
          ],
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Project',
        path: './content/projects/*.json',
        refs: {
          weitereProjekte: 'Project',
        },
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Story',
        path: './content/stories/*.json',
        refs: {
          weitereStories: 'Story',
        },
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Employee',
        path: './content/employees/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'OpenPosition',
        path: './content/open-positions/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Topic',
        path: './content/topics/*.json',
        refs: {
          projekte: 'Project',
          stories: 'Story',
        },
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'GatedAsset',
        path: './content/gated/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'LandingPage',
        path: './content/landing-pages/*.json',
        resolveAbsolutePaths: true,
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        exclude: ['/gated-content/**', '/lp/**'],
      },
    },
    {
      use: 'gridsome-plugin-plausible-analytics',
      options: {
        dataDomain: 'flavour-kommunikation.de',
        outboundLinkTracking: true,
      },
    },
  ],
};
