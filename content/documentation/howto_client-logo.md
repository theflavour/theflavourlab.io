<div style="max-width: 60rem; margin: 0 auto;">

# Anleitung: Neues Kundenlogo hinzufügen

> In dieser Anleitung geht es darum, wie ein neues Kundenlogo zur Verwendung in
> Logofriedhöfen und in Projekten bzw. Success Stories aufbereitet und
> importiert werden kann.


## 1. Das Logo aufbereiten

### Benötigte Dateien und Formate

Kundenlogos auf der Flavour-Website liegen in zwei Formaten vor:

- Einer bunten Version als SVG-Bild
- Einer monochromen Version als SVG-Code

Beide SVGs haben eine Höhe von **96px**, die Breite ergibt sich aus dem
Seitenverhältnis des Logos und kann daher variabel sein. Das monochrome Logo
muss *nicht* in einer weißen und einer schwarzen Version vorliegen, denn die
eigentliche Farbe wird später programmatisch gesetzt, eine schwarze Version
reicht vollkommen aus.


### SVG für das Web optimieren

Sowohl das SVG-Bild, aber insbesondere auch der SVG Code für das monochrome Logo
sollten über das Tool [SVGOMG](https://jakearchibald.github.io/svgomg/)
optimiert werden. Das verringert die Dateigröße enorm und entfernt überschüssige
Informationen, die z.B. von Adobe Illustrator eingefügt wurden.

Um ein SVG mit diesem Tool zu verbessern kann es einfach vom Finder in das Tool
gezogen, oder über den Button "Open SVG" in der Seitenleiste geöffnet werden.

![Ein Screenshot von SVGOMG](https://flavour-kommunikation.de/___documentation/svgomg_open.jpg)

Dann sollte das Logo im zentralen Bereich des Fensters angezeigt werden. Jetzt
bietet es sich an, heranzuzoomen, damit eventuelle Artefakte, die durch die
Komprimierung entstehen sichtbar werden.


![Ein Screenshot von SVGOMG mit geöffnetem Logo](https://flavour-kommunikation.de/___documentation/svgomg_full.jpg)


In der Toolbar an der rechten Seite des Fensters kann über den Slider "Precision"
der Kompressionsgrad des SVGs eingestellt werden. *Kleinere Werte bedeuten eine
höhere Kompression.* Hier sollte der niedrigstmögliche Wert eingestellt werden,
bei dem noch keine Artefakte (z.B. zackige Linien, eine veränderte Form, etc.)
auftreten. Für gewöhnlich sollte der Wert dieses Sliders bei 1 – 3 liegen. Für
einen besseren Vergleich mit dem Original kann über den Schalter "Show original"
das unkomprimierte SVG angezeigt werden.


![Ein Vergleich von zu niedriger vs genau richtiger Precision](https://flavour-kommunikation.de/___documentation/svgomg_precision.jpg)


In der Liste "Features" unterhalb des Sliders sollten **alle Optionen
aktiviert** sein, bis auf:

- `Remove xmlns`
- `Replace duplicate elements with links`

![Die aktiven Features in SVGOMG](https://flavour-kommunikation.de/___documentation/svgomg_features.jpg)

Das komprimierte SVG kann dann über den blauen Button unten rechts
heruntergeladen werden. Für das monochrome Logo bietet es sich allerdings an,
das SVG über den Button über dem Download-Button direkt als Text zu kopieren,
um ihn im CMS einzufügen.


![Die Downloadbuttons in SVGOMG](https://flavour-kommunikation.de/___documentation/svgomg_export.jpg)


## 2. Das Logo im CMS einfügen

Neue Logos können im CMS im Bereich "Verfügbare Kundenlogos" hinzugefügt werden.
In diesem Bereich befinden sich zwei Listen: eine Liste mit IDs und eine zweite
Liste mit den eigentlichen Logo-Elementen. Diese zweite Liste hat einen grünen
Button "Add Kundenlogo", mit dem ein neues Logo hinzugefügt werden kann.


![Der Add Kundenlogo Button in Forestry](https://flavour-kommunikation.de/___documentation/forestry_add_button.jpg)


In dem Dialog, der sich daraufhin öffnet, befinden sich drei Felder:

1. `ID` für die **einzigartige** Id des Logos – typischerweise der Name des Kunden
  in Kleinbuchstaben und ohne Sonderzeichen. Leerzeichen **müssen** mit
  Bindestrichen ersetzt werden. Aus "Schön Klinik" wird also z.B. "schoen-klinik".
2. `Logo` für die bunte Version des Logos, es kann entweder aus der Mediengalerie
  ausgewählt, oder neu hinzugefügt werden
3. `SVG Code` für den Code des monochromen SVGs. Hier kann der Text, der zuvor
  aus SVGOMG kopiert wurde eingefügt werden. Falls das monochrome SVG
  heruntergeladen wurde, kann dieses auch über "Öffnen mit" mit einem einfachen
  Texteditor geöffnet und der Code so kopiert werden.


![Die drei Felder zum anlegen eines neuen Logos](https://flavour-kommunikation.de/___documentation/forestry_steps.jpg)


**Wichtig:** der Code, der bei 3. in das Feld eingetragen wurde darf keine `fill`,
`stroke`, oder `stroke-width` Attribute enthalten. Falls also etwas wie
`<path fill="#000" …/>` auftauchen sollte, muss es so editiert werden, dass nur
`<path …/>` dort steht. **Ausnahme**: das erste `fill="none"` bei `<svg` *muss*
erhalten bleiben.


![Infografik, welche Attribute bestehen bleiben dürfen und welche nicht](https://flavour-kommunikation.de/___documentation/forestry_add_logo.jpg)


## 3. Das Logo zur Verwendung freigeben

Damit das Logo anschließend in Logofriedhöfen und Success Stories, bzw. Projekten
verwendet werden kann, muss die Id, die im zweiten Schritt in das "Id"-Feld
eingetragen wurde in die erste Liste auf der Seite eingefügt werden. Hier bitte
doppelt prüfen, ob die Id *exakt gleich* ist – oder einfach den Wert kopieren
und einfügen.


![Das Id Textfeld in Forestry](https://flavour-kommunikation.de/___documentation/forestry_add_id.jpg)


Abschließend müssen diese Änderungen noch über den "Speichern"-Button oben
rechts abgespeichert werden.


![Der Speichern-Button in Forestry](https://flavour-kommunikation.de/___documentation/forestry_save.jpg)

**Achtung**: es kann vorkommen, dass die Logos erst dann in einer Vorschau
angezeigt werden, wenn der Preview-Server neu gestartet wird. So lang die IDs und
der SVG-Code im angegebenen Format richtig eingetragen wurde, kann davon
ausgegangen werden, dass auf der veröffentlichten Website alles stimmt.

</div>
