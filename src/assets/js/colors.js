const apple = '#FB073E';
const aubergine = '#3F1D84';
const banana = '#FFBC05';
const bg = '#ffffff';
const bgGrey = '#F9F9FC';
const leaf = '#6DD73C';
const leafDark = '#009245';
const lemon = '#FBDC08';
const lime = '#D9E021';
const maracuja = '#B92FDD';
const orange = '#FE5608';
const pineapple = '#754C24';
const plum = '#312FDE';
const sky = '#1ECAF9';
const strawberry = '#D4145A';
const text = '#272A33';

const appleGradient = `linear-gradient(to bottom right, ${apple}, ${banana})`;
const aubergineGradient = `linear-gradient(to bottom right, ${plum}, ${aubergine})`;
const bananaGradient = `linear-gradient(to bottom right, ${lemon}, ${banana})`;
const leafDarkGradient = `linear-gradient(to bottom right, ${leafDark}, ${leaf})`;
const leafGradient = `linear-gradient(to bottom right, ${leaf}, ${lime})`;
const maracujaGradient = `linear-gradient(to bottom right, ${aubergine}, ${maracuja})`;
const orangeGradient = `linear-gradient(to bottom right, ${orange}, ${banana})`;
const pineappleGradient = `linear-gradient(to bottom right, ${pineapple}, ${banana})`;
const plumGradient = `linear-gradient(to bottom right, ${plum}, ${sky})`;
const strawberryGradient = `linear-gradient(to bottom right, ${strawberry}, ${apple})`;

export {
  apple,
  appleGradient,
  aubergine,
  aubergineGradient,
  banana,
  bananaGradient,
  bg,
  bgGrey,
  leaf,
  leafDark,
  leafDarkGradient,
  leafGradient,
  lemon,
  lime,
  maracuja,
  maracujaGradient,
  orange,
  orangeGradient,
  pineapple,
  pineappleGradient,
  plum,
  plumGradient,
  sky,
  strawberry,
  strawberryGradient,
  text,
};
